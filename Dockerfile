# # USE MULTI-STAGE BUILDS
# # Stage build
# FROM node:10 AS build
# WORKDIR /usr/temp
# COPY package*.json /usr/temp
# RUN npm ci --only=production && \
#     rm -f .npmrc

# Stage run
FROM node:10-alpine@sha256:dc98dac24efd4254f75976c40bce46944697a110d06ce7fa47e7268470cf2e28
USER node
WORKDIR /usr/app
COPY --chown=node:node --from=build /usr/temp/node_modules /usr/app/node_modules
COPY --chown=node:node . /usr/app
EXPOSE 8080
ENTRYPOINT ["/bin/sh", "docker-entrypoint.sh"]