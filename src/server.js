'use strict'
const express = require('express')
const Prometheus = require('prom-client')
const morgan = require('morgan')
const chalk = require('chalk')

const app = express()
app.disable('etag')
app.use(morgan('combined'))
const host = process.env.HOST || "0.0.0.0"
const hostname = process.env.HOSTNAME || "webserver"
const port = process.env.PORT || 8080
const env = process.env.ENV || "DEV"
const metricsInterval = Prometheus.collectDefaultMetrics()
const checkoutsTotal = new Prometheus.Counter({
  name: 'checkouts_total',
  help: 'Total number of checkouts',
  labelNames: ['payment_method']
})
const httpRequestDurationMicroseconds = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]  // buckets for response time from 0.1ms to 500ms
})

// Runs before each requests
app.use((req, res, next) => {
  res.locals.startEpoch = Date.now()
  next()
})

app.get('/', (req, res, next) => {
  setTimeout(() => {
    res.json({ message: 'Hello World!' })
    next()
  }, Math.round(Math.random() * 200))
})

/////////////////////////////////// Main function ///////////////////////////

// Get metrics for Prometheus
app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType)
  return res.end(Prometheus.register.metrics())
})

// For Liveness, Readiness probes
app.get('/healthz', (req, res) => {
  return res.status(200).json({ message: 'Health check' })
})

// For compresses the message api
app.get('/search', (req, res) => {
  const message = req.query.message
  console.log("message",message)
  if(typeof(message) === 'undefined') {
    return res.status(200).json({ message: "Message is undefined" })
  }
  if(message === ""){
    return res.status(200).json({ message: "Message is null" })
  }
  let result = compresses_message(message)
  return res.status(200).json({"message":result})
})

////////////////////////////////////////////////////////////////////////////////

// Error handler
app.use((err, req, res, next) => {
  res.statusCode = 500
  // Do not expose your error in production
  res.json({ error: err.message })
  next()
})

const server = app.listen(port, () => {
  console.log(chalk.gray('==================================================='))
  console.log(chalk.cyan(`\tEnvironment:  ${env}`))
  console.log(chalk.cyan(`\tInstance:  ${hostname}`))
  console.log(chalk.cyan(`\tHealth check:  ${host}:${port}/healthz`))
  console.log(chalk.cyan(`\tGet metrics:  ${host}:${port}/metrics`))
  console.log(chalk.gray('==================================================='))
})

// Graceful shutdown
process.on('SIGTERM', () => {
  clearInterval(metricsInterval)
  server.close((err) => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
    process.exit(0)
  })
})

function compresses_message(message) {
  let count = 1
  let temp = message[0]
  let result = ""
  for(let i = 1; i < message.length; i++) {
    if(message[i] === temp) {
      count += 1
    }
    else {
      if(count === 1) {
        result = result + temp
      }
      else {
        result = result + temp + count.toString()
      }
      temp = message[i]
      count = 1
    }
    if(i === message.length-1){
      if(count === 1) {
        result = result + temp
      }
      else {
        result = result + temp + count.toString()
      }
    }
  }
  return result
}